﻿namespace NumbersStrings
{
    public class Calculator
    {
        public Calculator()
        {
        }

        public int AddInt(int a, int b)
        {
            return a + b;
        }

        public double AddDouble(double a, double b)
        {
            return a + b;
        }
    }
}
