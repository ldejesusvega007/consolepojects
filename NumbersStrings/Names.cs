﻿namespace NumbersStrings
{
    public class Names
    {
        public Names()
        {
        }

        public string MakeFullName(string firstName, string lastName)
        {
            return $"{firstName} {lastName}";
        }
    }
}
