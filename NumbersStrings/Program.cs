﻿using System;

namespace NumbersStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator calculator = new Calculator();
            Names names = new();

            Console.WriteLine($"Ejecutar Makefullname metodo");
            Console.WriteLine($"Este es el nombre {names.MakeFullName("Regina", "King")}");
            Console.WriteLine($"Nuevo nombre {names.MakeFullName("Luna", "Palma")}");
            Console.WriteLine("");
            Console.WriteLine($"Ejecutar Suma de Enteros");
            Console.WriteLine($"resultado de sumar 1 + 1 : {calculator.AddInt(1, 1)}");
            Console.WriteLine($"resultado de sumar 2 + 2 : {calculator.AddInt(2, 2)}");
            Console.WriteLine("");
            Console.WriteLine($"Ejecutar suma de Dobles");
            Console.WriteLine($"resultado de sumar 1.1 + 1.1 : {calculator.AddDouble(1.1, 1.1)}");
            Console.WriteLine($"resultado de sumar 2.2 + 2.2 : {calculator.AddDouble(2.2, 2.2)}");
        }
    }
}
